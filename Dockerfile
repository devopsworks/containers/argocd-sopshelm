ARG ARGOCD_VERSION=v1.8.2

FROM golang:1.15
ARG SOPSHELM_VERSION=v1.2.2
RUN GO111MODULE=on go get gitlab.com/ittennull/sopshelm@${SOPSHELM_VERSION}

# ---
FROM argoproj/argocd:${ARGOCD_VERSION}
COPY --from=0 /go/bin /usr/local/bin/
USER root  
RUN cd /usr/local/bin && \
    mv helm helm.bin && \
    mv sopshelm helm
ENV HELM_BINARY_PATH=/usr/local/bin/helm.bin
USER argocd
